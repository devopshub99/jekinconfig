def gitlab_repos = [
    'userservice',
    'productservice'
]

// create a pipeline job for each of the repos and for each feature branch.
for (gitlab_repo in gitlab_repos)
{
  multibranchPipelineJob("${gitlab_repo}-ci") {
    // configure the branch / PR sources
    branchSources {
      branchSource {
        source {
          gitlab {
            serverName('gitlab.com')
            projectOwner("devopshub99/${gitlab_repo}")
            projectPath()
            traits { // Declare Behaviours
              gitLabBranchDiscovery {
                strategyId(3)
              }
            }
          }
        }
        strategy {
          defaultBranchPropertyStrategy {
            props {
              // keep only the last 10 builds
              buildRetentionBranchProperty {
                buildDiscarder {
                  logRotator {
                    daysToKeepStr("-1")
                    numToKeepStr("10")
                    artifactDaysToKeepStr("-1")
                    artifactNumToKeepStr("-1")
                  }
                }
              }
            }
          }
        }
      }
    }

    // check every 5 minute for scm changes as well as new / deleted branches
    triggers {
      periodicFolderTrigger {
        interval('5m')
      }
    }
    // don't keep build jobs for deleted branches
    orphanedItemStrategy {
      discardOldItems {
        numToKeep(-1)
      }
    }
  }
}
